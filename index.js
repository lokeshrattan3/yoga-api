import express, { response } from "express";
// import dotenv from 'dotenv'
import mysql from "mysql";
import cors from "cors";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import cookieParser from "cookie-parser";

const salt = 10;


// const proxy = require("http-proxy-middleware");
const app = express();

app.use(express.static('public'));
app.use(cors({
    origin: "http://localhost:3000",
    methods: ["GET", "POST", "PUT", "DELETE"],
    credentials: true,
}));
app.use(express.json())
app.use(cookieParser());
// app.use("/api", proxy({target: 'http://localhost:8080', changeOrigin: true}));

const db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    port: "3306",
    database: "yoga-backend"
});

db.connect((err) => {
    if (err) {
        console.error("Error connecting to database:", err);
    } else {
        console.log("Connected to database");
    }
});

const verifyUser = (req, res, next) => {
    const token = req.cookies.token;
    if (!token) {
        return res?.json({ Error: "You are not logged in, please login" });
    } else {
        jwt.verify(token, "unique-token-key", (err, decoded) => {
            if (err) {
                return res?.json({ Error: "Invalid token" });
            } else {
                req.name = decoded.name;
                req.userId = decoded.id; // Add user ID to the request
                next();
            }
        });
    }
};

app.get('/yoga-api', verifyUser, (req, res) => {
    return res.json({ status: "success", name: req.name, userId: req.userId });
});

app.post('/yoga-api/signup', (req, res) => {
    const sql = "INSERT INTO users (`name`, `email`, `password`) VALUES (?)";
    
    bcrypt.hash(req.body.password.toString(), salt, (err, hash) => {
        if (err) return res.json({ Error: "Error in Password hashing" });

        const values = [
            req.body.name,
            req.body.email,
            hash
        ];

        db.query(sql, [values], (err, result) => {
            if (err) {
                console.error("Error in data inserting into server:", err);
                return res.json({ Error: "Error in data inserting into server" });
            }
            
            return res.json({ status: "success" });
        });
    });
});

app.post('/yoga-api/login', (req, res) => {
    const sql = "SELECT * FROM users WHERE email = ?";

    db.query(sql, [req?.body?.email], (err, data) => {
        if (err) return res?.json({ Error: "Error in server during login" });
        if (data?.length > 0) {
            bcrypt.compare(req?.body?.password?.toString(), data[0]?.password, (err, response) => {
                if (err) return res?.json({ Error: "Error in encrypting password" });
                if (response) {
                    const name = data[0].name;
                    const id = data[0].id;
                    const token = jwt?.sign({ name, id }, "unique-token-key", { expiresIn: "1d" });
                    res?.cookie('token', token);
                    res?.cookie('userId', id)
                    return res?.json({ status: "success" });
                } else {
                    return res?.json({ Error: "Incorrect password" });
                }
            });
        } else {
            return res?.json({ Error: "User Not Exists" });
        }
    });
});


app.get('/yoga-api/logout', (req, res) => {
    res.clearCookie('token');
    res.clearCookie('userId');

    return res.json({success: true});
})

app.post('/yoga-api/contact-us', (req, res) => {

    const sql = "INSERT INTO contact_us_query (`name`, `email`, `message`, `uid`) VALUES (?)";
    const values = [
        req.body.name,
        req.body.email,
        req.body.message,
        req.body.uid
    ];

    db.query(sql, [values], (err, result) => {
        if (err) {
            console.error("Error in data inserting into server:", err);
            return res.json({ Error: "Error in data inserting into server" });
        }
        
        return res.json({ status: "success" });
    });
})

app.get(`/yoga-api/contact-us/:id`, (req, res) => {
    const sql = "SELECT * FROM contact_us_query WHERE uid = ?";
    const id = req.params.id;
    db.query(sql, [id], (err, data) => {
        if (err) return res.json({ Error: "Error in data fetching" })
        return res.json({data});
    })

})

app.post(`/yoga-api/update-query/:id`, (req, res) => {
    const sql = "UPDATE contact_us_query SET `name` = ?, `email` = ?, `message` = ? WHERE id = ?";
    
    const values = [
        req.body.name,
        req.body.email,
        req.body.message
    ]
    const id = req.params.id;
    db.query(sql, [...values, id], (err, data) => {
        if (err) return res.json({ Error: "Error in data updating" })
        return res.json({data});
    })

})


app.delete(`/yoga-api/contact-us/delete/:id`, cors(), (req, res) => {
    const sql = "DELETE FROM contact_us_query WHERE `id` = ?";

    const id = req.params.id;
    db.query(sql, [id], (err, data) => {
        if (err) {
            console.error("Error in data deleting:", err);
            return res.json({ Error: "Error in data deleting" });
        }
        return res.status(204).send();  // Send a successful status without response data
    });
});


app.post('/yoga-api/personal-info', (req, res) => {

    const sql = "INSERT INTO personal_info (`name`, `weight`, `height`, `uid`) VALUES (?)";
    const values = [
        req.body.name,
        req.body.weight,
        req.body.height,
        req.body.uid
    ];

    db.query(sql, [values], (err, result) => {
        if (err) {
            console.error("Error in data inserting into server:", err);
            return res.json({ Error: "Error in data inserting into server" });
        }
        
        return res.json({ status: "success" });
    });
})

app.get(`/yoga-api/personal-info/:id`, (req, res) => {
    const sql = "SELECT * FROM personal_info WHERE uid = ?";
    const id = req.params.id;
    db.query(sql, [id], (err, data) => {
        if (err) return res.json({ Error: "Error in data fetching" })
        return res.json({data});
    })

})

app.post(`/yoga-api/update-info/:id`, (req, res) => {
    const sql = "UPDATE personal_info SET `name` = ?, `weight` = ?, `height` = ? WHERE id = ?";
    
    const values = [
        req.body.name,
        req.body.weight,
        req.body.height
    ]
    const id = req.params.id;
    db.query(sql, [...values, id], (err, data) => {
        if (err) return res.json({ Error: "Error in data updating" })
        return res.json({data});
    })

})



app.listen(5000, () => {
    console.log("Running ");
});
